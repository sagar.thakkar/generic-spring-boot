package com.neosoftmail.entity;
/*
 * Sagar Thakkar created on
 * 29/10/21 4:14 pm inside the package -
 * com.neosoftmail.entity
 */

import lombok.Data;

@Data
public class TestDTO extends BaseEntity<Long, String>{

    private String nameGiven;
    private String typeDisplay;
}
