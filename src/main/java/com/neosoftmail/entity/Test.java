package com.neosoftmail.entity;
/*
 * Sagar Thakkar created on
 * 28/10/21 1:05 pm inside the package -
 * com.neosoftmail.entity
 */

import lombok.Data;

import javax.persistence.Entity;

@Entity
@Data
public class Test extends BaseEntity<Long, String>{

    private String name;

    private String type;

}
