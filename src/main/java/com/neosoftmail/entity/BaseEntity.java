package com.neosoftmail.entity;
/*
 * Sagar Thakkar created on
 * 28/10/21 12:25 pm inside the package -
 * com.neosoftmail.entity
 *
 *
 * Base Entity which poses the basic configuration required in all entities
 * @
 */

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@EntityListeners(AuditingEntityListener.class)
@JsonIgnoreProperties(
        value = {"createdAt", "updatedAt"},
        allowGetters = true
)
@Data
@MappedSuperclass
public class BaseEntity<T extends Serializable ,U> implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private T id;

    @CreatedBy
    @Column(updatable = false)
    protected U createdBy;

    @CreatedDate
    @Temporal(TemporalType.TIMESTAMP)
    @Column(updatable = false)
    @JsonFormat(pattern="yyyy-MM-dd")
    protected Date createdDate;

    @LastModifiedBy
    protected U lastModifiedBy;

    @LastModifiedDate
    @Temporal(TemporalType.TIMESTAMP)
    @JsonFormat(pattern="yyyy-MM-dd")
    protected Date lastModifiedDate;


}
