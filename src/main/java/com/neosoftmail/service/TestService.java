package com.neosoftmail.service;
/*
 * Sagar Thakkar created on
 * 29/10/21 12:48 pm inside the package -
 * com.neosoftmail.service
 *
 *
 *
 * This service implements the BaseService,
 * which has required configuration and Type bounding.
 *
 */

import com.neosoftmail.entity.Test;
import org.springframework.stereotype.Service;

@Service
public interface TestService extends BaseService<Test, Long>{
    Test additionalMethod(Test test);
}
