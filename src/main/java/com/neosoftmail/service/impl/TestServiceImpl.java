package com.neosoftmail.service.impl;
/*
 * Sagar Thakkar created on
 * 29/10/21 4:04 pm inside the package -
 * com.neosoftmail.service.impl
 *
 *
 * Service implementation of TestService @Interface.
 *
 */

import com.neosoftmail.entity.Test;
import com.neosoftmail.service.TestService;

import java.util.List;

public class TestServiceImpl implements TestService {
    // TODO: Implemention logic  required to be added

    @Override
    public Test save(Test test) {
        return null;
    }

    @Override
    public Test getById(Long aLong) {
        return null;
    }

    @Override
    public Test update(Test test) {
        return null;
    }

    @Override
    public void deleteById(Long aLong) {

    }

    @Override
    public List<Test> findAll() {
        return null;
    }

    @Override
    public Test additionalMethod(Test test) {
        return null;
    }
}
