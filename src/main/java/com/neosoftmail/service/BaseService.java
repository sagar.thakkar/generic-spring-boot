package com.neosoftmail.service;
/*
 * Sagar Thakkar created on
 * 29/10/21 12:16 pm inside the package -
 * com.neosoftmail
 */

import java.util.List;

/**
 * This is a BaseService interface
 * @used for providing basic structure for specific service.
 * @param <T> can be any entity class
 * @param <ID> can be @param ID of that class which can be String, Long or Integer.
 */
public interface BaseService<T, ID> {

    T save(T t);
    T getById(ID id);
    T update(T t);
    void deleteById(ID id);
    List<T> findAll();

}
