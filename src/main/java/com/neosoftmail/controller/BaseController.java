package com.neosoftmail.controller;

import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;

import java.util.List;



public interface BaseController<T, ID> {

    @GetMapping
    public T getById(ID id);

    @PostMapping
    public T save(T t);

    @DeleteMapping
    public T deleteById(ID id);

    @PutMapping
    public T update(T t);

    @GetMapping
    public List<T> findAll();

}
