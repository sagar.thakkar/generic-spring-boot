package com.neosoftmail.controller;
/*
 * Sagar Thakkar created on
 * 29/10/21 4:07 pm inside the package -
 * com.neosoftmail.controller
 */

import com.neosoftmail.entity.Test;
import com.neosoftmail.entity.TestDTO;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class TestController implements BaseController<TestDTO, Long>{
    // TODO: Implement required logic inside methods
    @Override
    public TestDTO getById(Long aLong) {
        return null;
    }

    @Override
    public TestDTO save(TestDTO testDTO) {
        return null;
    }

    @Override
    public TestDTO deleteById(Long aLong) {
        return null;
    }

    @Override
    public TestDTO update(TestDTO testDTO) {
        return null;
    }

    @Override
    public List<TestDTO> findAll() {
        return null;
    }


}
