package com.neosoftmail.dao.baseAuditing;
/*
 * Sagar Thakkar created on
 * 29/10/21 4:10 pm inside the package -
 * com.neosoftmail.dao
 */


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.AuditorAware;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@Configuration
@EnableJpaAuditing(auditorAwareRef = "auditorAware")
public class JpaConfig {

    @Bean
    public AuditorAware<String> auditorAware() {return new AuditorAwareImpl(); }
}
