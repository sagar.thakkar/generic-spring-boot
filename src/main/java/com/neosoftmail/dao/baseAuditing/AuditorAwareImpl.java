package com.neosoftmail.dao.baseAuditing;
/*
 * Sagar Thakkar created on
 * 29/10/21 4:11 pm inside the package -
 * com.neosoftmail.dao
 */

import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<String> {

    @Override
    public Optional<String> getCurrentAuditor() {
      return Optional.of("Admin");
    }
}
