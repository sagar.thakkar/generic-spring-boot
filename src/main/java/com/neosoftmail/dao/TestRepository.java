package com.neosoftmail.dao;
/*
 * Sagar Thakkar created on
 * 28/10/21 1:26 pm inside the package -
 * com.neosoftmail.dao
 */

import com.neosoftmail.entity.Test;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestRepository extends JpaRepository<Test, Long> {
}
