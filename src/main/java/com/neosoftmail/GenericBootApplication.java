package com.neosoftmail;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GenericBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(GenericBootApplication.class, args);
    }

}
